// Init db
const firebaseConfig = {
    apiKey: "AIzaSyB23MK9pWZ52Y7-amWaqt-rlpY078lZmZ0",
    authDomain: "d4green.firebaseapp.com",
    databaseURL: "https://d4green.firebaseio.com",
    projectId: "d4green",
    storageBucket: "d4green.appspot.com",
    messagingSenderId: "909635772732",
    appId: "1:909635772732:web:09e2f796ef5f6efe17be39"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// Init Firestore
let db = firebase.firestore();

// Load JSON
//  Access
// let requete = new XMLHttpRequest();
// requete.open("GET", "../datasets/access.json", false);
// requete.send();
// if (requete.readyState == 4 && requete.status == 200) {
//     let access = JSON.parse(requete.responseText);
//     access.users.map((user) => {
//         db.collection("users").add(user);
//     })
// }
// Locataires
// let requete = new XMLHttpRequest();
// requete.open("GET", "../datasets/locataires.json", false);
// requete.send();
// if (requete.readyState == 4 && requete.status == 200) {
//     let access = JSON.parse(requete.responseText);
//     console.log(access.locataires);
//     access.locataires.map((locataire) => {
//         console.log(locataire);
//         db.collection("locataires").add(locataire);
//     })
// }
// Proprietaires
// let requete = new XMLHttpRequest();
// requete.open("GET", "../datasets/proprietaires.json", false);
// requete.send();
// if (requete.readyState == 4 && requete.status == 200) {
//     let access = JSON.parse(requete.responseText);
//     console.log(access.proprietaires);
//     access.proprietaires.map((proprietaire) => {
//         console.log(proprietaire);
//         db.collection("proprietaires").add(proprietaire);
//     })
// }
// Logements
// let requete = new XMLHttpRequest();
// requete.open("GET", "../datasets/logements.json", false);
// requete.send();
// if (requete.readyState == 4 && requete.status == 200) {
//     let access = JSON.parse(requete.responseText);
//     console.log(access.logements);
//     access.logements.map((logement) => {
//         console.log(logement);
//         db.collection("logements").add(logement);
//     })
// }
// Datas
// let requete = new XMLHttpRequest();
// requete.open("GET", "../datasets/datas.json", false);
// requete.send();
// if (requete.readyState == 4 && requete.status == 200) {
//     let access = JSON.parse(requete.responseText);
//     console.log(access.datas);
//     access.datas.map((data) => {
//         console.log(data);
//         db.collection("datas").add(data);
//     })
// }