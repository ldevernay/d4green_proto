// const firebase = require("firebase");
// // Required for side-effects
// require("firebase/firestore");

// Init db
const firebaseConfig = {
    apiKey: "AIzaSyB23MK9pWZ52Y7-amWaqt-rlpY078lZmZ0",
    authDomain: "d4green.firebaseapp.com",
    databaseURL: "https://d4green.firebaseio.com",
    projectId: "d4green",
    storageBucket: "d4green.appspot.com",
    messagingSenderId: "909635772732",
    appId: "1:909635772732:web:09e2f796ef5f6efe17be39"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
let db = firebase.firestore();

let getFoyerData = (name) => {
    let labels = [];
    db.collection("datas")
        .where("Foyer", "==", name)
        .get()
        .then((query) => {
            query.forEach((doc) => {
                let data = doc.data();
                let foyer = data["Foyer"];
                delete data["Foyer"];
                labels = Object.keys(data);
                let values = Object.values(data);
                const stuff = {
                    labels,
                    datasets: [
                        {
                            name: `Foyer ${foyer}`, type: "line",
                            values
                        }
                    ]
                };
                const chart = new frappe.Chart('#chart', {
                    title: "Consommation d'électricité",
                    data: stuff,
                    type: 'line',
                    height: 250,
                    colors: ['#7cd6fd', '#743ee2']
                });
                //chart.export();
            });
        });
};

let getAllData = () => {
    let labels = [];
    let datasets = [];
    db.collection("datas")
        .get()
        .then((query) => {
            query.forEach((doc) => {
                let data = doc.data();
                let foyer = data["Foyer"];
                delete data["Foyer"];
                labels = Object.keys(data);
                let values = Object.values(data);
                datasets.push({
                    name: `Foyer ${foyer}`, type: "line",
                    values
                })
            });
            const stuff = {
                labels,
                datasets
            };
            const chart = new frappe.Chart('#chart', {
                title: "Consommation d'électricité",
                data: stuff,
                type: 'line',
                height: 250,
                colors: ['#7cd6fd', '#743ee2']
            });
        });
}